---
title: Biographical data sheets
---

## Overview

This page includes biographical data sheets for key personnel in Volkis.

## Matthew Strahan - Managing Director

13 years of dedicated, cyber security experience in designing security strategies, security architecture, implementing cyber security frameworks and standards, developing policy, procedure, standards, and processes, technology selection and implementation, incident response, and penetration testing. Carries industry qualifications including CISSP, CISM, CISA and CGEIT, holds a Bachelor of Computer Science and currently undertaking a Master of Business Administration.

6 years of team leadership and management experience as the leader of operations and principal consultant, including leading internal IT, including support, IT transformations and an ISO27001 implementation. Led the implementation of internal systems and process improvement.

Available on the following social media sites:

- [LinkedIn](https://www.linkedin.com/in/matthew-strahan/)
- [Twitter](https://twitter.com/matt_strahan/)

### Employment history

| Company          | Role                       | From |   To    |
| ---------------- | -------------------------- | :--: | :-----: |
| Volkis           | Managing Director          | 2019 | Present |
| Content Security | Senior Security Strategist | 2019 |  2019   |
| Content Security | Operations Manager         | 2017 |  2019   |
| Content Security | Principal Consultant       | 2012 |  2017   |
| Securus Global   | Security Consultant        | 2007 |  2011   |

### Education

| Institution          | Degree                 | Obtained |
| -------------------- | ---------------------- | :------: |
| University of NSW    | BSc (Computer Science) |   2007   |
| Macquarie University | MBA                    | Ongoing  |

### Certifications

- Certified Information Security Auditor (CISA)
- Certified Information Security Manager (CISM)
- Certified in Governance of Enterprise IT (CGEIT)
- Certified Ethical Hacker (CEH)
- Certified Information Systems Security Professional (CISSP)

### Key skills

- Experienced in business and technical security
- Penetration testing, including internal, external, wireless, web app, desktop app
- Business security, including security architecture, governance, management, policy, procedure, and framework
- Credit card security, including PCI DSS
- General IT, including transformation, governance, programming, software engineering
  Presented at industry bodies such as CPA Forum, Power Housing Risk and CFO meetings, LGASA

## Alexei Doudkine - Offensive Director

Penetration tester, team leader, and security consultant with a decade of experience in building security systems and providing high level security consultancy. Worked as lead of the Threat Ops team, providing penetration testing, security consulting, IR and security training at Content Security.

Built the Red Team function at Content Security, providing advanced security threat style engagements. This included building infrastructure required for command and control for such engagements.

Built the “Hands-on Hacking” security education course, teaching members of the NSW Police and Department of Justice around hacking techniques.

Created hacking tools for security professionals including [RidRelay](https://github.com/skorov/ridrelay).

Available on the following social media sites:

- [LinkedIn](https://www.linkedin.com/in/alexei-doudkine/)
- [Twitter](https://twitter.com/skorov8)

### Employment history

| Company          | Role                 | From |   To    |
| ---------------- | -------------------- | :--: | :-----: |
| Volkis           | Offensive Director   | 2019 | Present |
| Content Security | Principal Consultant | 2017 |  2019   |
| Content Security | Security Consultant  | 2013 |  2017   |
| Pacom Systems    | Software Engineer    | 2010 |  2013   |

### Education

| Institution       | Degree                 | Obtained |
| ----------------- | ---------------------- | :------: |
| University of NSW | BSc (Computer Science) |   2010   |

### Certification

- Certified Information Systems Security Professional (CISSP)
- Offensive Security Certified Professional (OSCP)
- CREST Registered Tester (CRT)
- GIAC Certified Forensic Examiner (GCFE)
- Payment Card Industry - Qualified Security Assessor (PCI-QSA)

### Key skills

- Red team, Penetration testing, including internal, external, wireless, web app, mobile app
- Social engineering and physical security, including manipulating security badges and access control systems
- Presenting, including creating full security education courses
- Infrastructure development and systems administration
- Security design and architecture
- Business security, including payment card industry, risk assessments
