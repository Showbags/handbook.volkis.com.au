---
title: Volkis goodies
---

Find all the goodies, free stuff and giveaways on this page. Usage is completely optional.

## Volkis wallpapers

### v2.0

![Volkis wallpaper 1920x1080](/assets/img/Volkis_Wallpaper_2.0_1080p.png)

- [Volkis wallpaper 1920x1080](/assets/img/Volkis_Wallpaper_2.0_1080p.png)
- [Volkis wallpaper 2560×1440](/assets/img/Volkis_Wallpaper_2.0_QHD.png)
- [Volkis wallpaper 3840×2160](/assets/img/Volkis_Wallpaper_2.0_4K.png)
